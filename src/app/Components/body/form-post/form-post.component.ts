import { Iformpost } from './models/Iformpost';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-post',
  templateUrl: './form-post.component.html',
  styleUrls: ['./form-post.component.scss']
})
export class FormPostComponent implements OnInit {

  @Output() newForm = new EventEmitter<any>();

  public formpost: FormGroup = null;
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) { 
    this.formpost = this.formBuilder.group({
      name:['',[Validators.required, Validators.minLength(2)]],
      apellidos:['',[Validators.required, Validators.minLength(2)]], 
      personas:['', [Validators.required, Validators.minLength(1)]],
      email:['', [Validators.required, Validators.email]],
      phone:['',[Validators.required, Validators.minLength(9)]],
    })
  }

  ngOnInit(): void {/*Empty*/}

  public onSubmit(): void {
    this.submitted = true;
    if(this.formpost.valid){

    const responseFormPost: Iformpost = {
      name: this.formpost.get('name').value,
      apellidos: this.formpost.get('apellidos').value,
      personas: this.formpost.get('personas').value,
      email: this.formpost.get('email').value,
      phone: this.formpost.get('phone').value,
    };
    
    console.log(responseFormPost);
    this.submitted = false;
    this.formpost.reset();
    this.newForm.emit(responseFormPost);
   }
  
  } 

}
