import { Component, OnInit } from '@angular/core';
import {Gallery, Photos} from './model/Ifotos';
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
 Burguers: Gallery | any={};
 Photo1: Photos | any={};
 Photo2: Photos | any={};
 Photo3: Photos | any={};
 Photo4: Photos | any={};
 Photo5: Photos | any={};
 Photo6: Photos | any={};
  constructor() { 
    
    this.Photo1.img = 'https://tse1.mm.bing.net/th?id=OIP.35mnAsmuN1EWZAfnhOB2BgHaH4&pid=Api&P=0&w=300&h=300';
    this.Photo1.imgAlt = 'cartel hamburguesa';

    this.Photo2.img ='https://tse1.mm.bing.net/th?id=OIP.lM_FLz_3ba5UW_ZQoKk1nwHaJb&pid=Api&P=0&w=300&h=300';
    this.Photo2.imgAlt = 'cartel hamburguesa';

    this.Photo3.img ='https://tse4.mm.bing.net/th?id=OIP.xkL5RFGwJAV2jDVNOko8TAHaJA&pid=Api&P=0&w=300&h=300';
    this.Photo3.imgAlt = 'cartel hamburguesa';

    this.Photo4.img='https://tse1.mm.bing.net/th?id=OIP.VBi_2DmT9xcI2oEY-dUe0gHaHv&pid=Api&P=0&w=300&h=300';
    this.Photo4.imgAlt = 'cartel hamburguesa';

    this.Photo5.img='https://tse1.mm.bing.net/th?id=OIP.gnlKM2va0zluGI6pJFN07AHaJb&pid=Api&P=0&w=300&h=300';
    this.Photo5.imgAlt = 'cartel hamburguesa';

    this.Photo6.img='https://tse1.mm.bing.net/th?id=OIP.vE8Uhk9F6WNnCXrYjT70fQHaI-&pid=Api&P=0&w=300&h=300';
    this.Photo6.imgAlt = 'cartel hamburguesa';

    
  }

  ngOnInit(): void {
  }

}
