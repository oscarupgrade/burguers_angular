export interface Gallery{
    img: Photos;
}

export interface Photos{
   photo: string;
   photoAlt: string;
}