import { Iformpost } from './../form-post/models/Iformpost';
import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() responseFormPost: Iformpost;

  constructor() { }

  ngOnInit(): void {
    
  }

}
