import { Iformpost } from './form-post/models/Iformpost';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {
array: [] | any;
  constructor() { 
    this.array=[{
      name: '',
      apellidos:'',
      email:'',
      phone: '',
      personas: ''
    }];
  }

  ngOnInit(): void {
  }

  setForm(form: Iformpost ){
    this.array.push(form);
  }
}
