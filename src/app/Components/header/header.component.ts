import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  logo: string;
  logoAlt: string;
  phone: number;

  constructor() { 
    this.logo = '/assets/img/logoprincipal.jpg';
    this.logoAlt = 'Logo restaurante';
    this.phone = 632456548;
    
  }

  ngOnInit(): void {
  }

}
