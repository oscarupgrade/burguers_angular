
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { Icontact } from './models/Icontact';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public contact: FormGroup = null;
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) { 
    this.contact= this.formBuilder.group({
      name: ['',[Validators.required, Validators.minLength(2)]],
      email: ['',[Validators.required, Validators.email]],
      coment: ['',[Validators.required, Validators.maxLength(20)]],
    })
  }

  ngOnInit(): void {/*Empty*/}

  public onSubmit(): void {
    this.submitted = true;
    if(this.contact.valid){

    const responseContact: Icontact = {
      name: this.contact.get('name').value,
      email: this.contact.get('email').value,
      coment: this.contact.get('coment').value,
    };
    console.log(responseContact);
    this.submitted = false;
    this.contact.reset();
   }
   
  } 

}
